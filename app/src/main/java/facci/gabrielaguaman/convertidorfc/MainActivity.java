package facci.gabrielaguaman.convertidorfc;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private EditText edt1;
    private TextView txt1;
    private CheckBox cbx1, cbx2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edt1 = (EditText) findViewById(R.id.edt1);
        txt1 = (TextView) findViewById(R.id.txt1);
        cbx1 = (CheckBox) findViewById(R.id.cbx1);
        cbx2 = (CheckBox) findViewById(R.id.cbx2);
    }

    public void convertir (View view) {
        String val = edt1.getText().toString();
        int num = Integer.parseInt(val);

        if (cbx1.isChecked() == true) {
            double conv1 = (num - 32) /1.8;
            String res = String.valueOf(conv1);
            txt1.setText(res);
        } else
        if (cbx2.isChecked() == true) {
            double conv2 = (num * 1.8) + 32;
            String res = String.valueOf(conv2);
            txt1.setText(res);
        }
    }
}
